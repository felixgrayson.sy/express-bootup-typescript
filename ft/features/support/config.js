const _ = require('lodash');

const settings = require('../../settings/appsettings.json');
const secrets = require('../../secrets/secrets.json');

module.exports = _.merge({}, settings, secrets);