const axios = require('axios');
const { defineSupportCode } = require('cucumber');

const url = process.env.E2E
  ? `https://${process.env.ENV_PREFIX}ms.centrality.ai`
  : `http://${process.env.APP_HOSTNAME || 'localhost'}:${process.env.APP_PORT ||
      8080}`;

const settings = {
  baseURL: `${url}/event-tracker/api`,
  timeout: 1000,
  headers: {
    'Content-Type': 'application/json'
  }
};

function world() {
  const client = axios.create(settings);
  this.get = (path, config) => client.get(path, config);
  this.post = (path, data, config) => client.post(path, data, config);
  this.put = (path, data, config) => client.put(path, data, config);
  this.delete = (path, config) => client.delete(path, config);
}

defineSupportCode(function({ setWorldConstructor }) {
  setWorldConstructor(world);
});
process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at:', p, 'reason:', reason);
});
