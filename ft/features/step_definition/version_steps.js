const { defineSupportCode } = require('cucumber');
const expect = require('chai').expect;

defineSupportCode(function ({ Given, When, Then }) {

  Given('an app is running', function(callback){
    // Write code here that turns the phrase above into concrete actions
    callback(null, 'skipped');
  });

  When('making http request on {string}', function (path) {
    this.requestResult = {};
    this.status = 400;
    return this.get(path, {}).then((result => {
      this.requestResult = result;
      this.status = this.requestResult.status;
    })).catch((err) => {
      this.requestResult = err.response;
      this.status = this.requestResult.status;
    });
  });

  Then('status {int} is returned', function (int) {
    // Write code here that turns the phrase above into concrete actions
    expect(this.status).to.equal(int);
  });

  Then('Version matches correct number', function () {
    expect(this.requestResult.data.buildNumber).to.equal(process.env.BUILD_TAG);
  });
});
