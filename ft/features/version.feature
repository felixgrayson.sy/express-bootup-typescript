e2e
Feature: Version API
Version api returns service name and build number

@e2e
Scenario: Call Version API
  Given an app is running
  When making http request on '/public/version'
  Then status 200 is returned
  And Version matches correct number