/** @format */

import { Request, Response, NextFunction } from 'express';
import boom from '@hapi/boom';
import Winston from '../../services/logger';

const logger = Winston.getInstance().logger;

export default function errorHandler(
  err,
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (!boom.isBoom(err)) {
    if (err.name === 'UnauthorizedError') {
      res
        .status(401)
        .json({
          statusCode: 401,
          error: 'Unauthorized',
          message: 'invalid_token',
        });
      return next();
    }

    if (err.status) {
      logger.error(`[Server].[Error]: ${JSON.stringify(err)}`);
      res
        .status(err.status)
        .json({
          statusCode: err.status,
          error: err.statusText,
          message: err.message,
        });
      return next();
    }

    logger.error(`[Server].[Error]: ${JSON.stringify(err)}`);
    res.status(500).json({ statusCode: 500, message: 'Internal Server Error' });
    return next();
  } else {
    logger.error(`[Server].[Error]: ${JSON.stringify(err)}`);
    res.status(err.output.statusCode).json(err.output.payload);
  }
}
