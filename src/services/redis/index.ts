import Redis, { RedisClient, ClientOpts } from 'redis';
import BlueBird from 'bluebird';
import Logger from '../logger';
import { utcNow } from '../utils/data-utils';

BlueBird.promisifyAll(Redis);
const logger = Logger.getInstance().logger;

export interface IRedisCli {
  client: RedisClient;
  options: ClientOpts;
  connect: () => Promise<RedisClient>;
  readonly isConnected: boolean;
}

export class RedisCli implements IRedisCli {
  private static instance: RedisCli;
  client: RedisClient;
  options: ClientOpts;

  private constructor( redisOptions: ClientOpts ) {
    this.options = redisOptions;
  }

  static getInstance( redisOptions: ClientOpts ) {
    if ( !RedisCli.instance ) {
      RedisCli.instance = new RedisCli(redisOptions);
    }
    return RedisCli.instance;
  }

  connect = async (): Promise<RedisClient> => {
    this.options.retry_strategy = ( opts ) => {
      if (opts.error && opts.error.code === 'ECONNREFUSED') {
        // End reconnecting on a specific error and flush all commands with
        // a individual error
        return new Error('The Redis Server refused the connection');
      }

      if (opts.total_retry_time > 1000 * 60 * 60) {
        return new Error('Retry time exhausted');
      }

      if (opts.attempt > 10) {
        return new Error('attempt reached the limitation.');
      }

      return Math.min(opts.attempt * 100, 3000);
    };

    const redisClient = Redis.createClient(this.options);

    redisClient.on('end', () => {
      logger.info(`[Redis].[Info]: Redis server connection has closed on ${utcNow()}`);
      process.exit(-1);
    });

    redisClient.on('error', ( err ) => {
      logger.warn(`[Redis].[Error]: ${JSON.stringify(err)}`);
    });

    return new Promise<RedisClient>((resolve => redisClient.on('connect', () => {
      logger.info(`[Redis].[Info]: Redis server connect to DB[${this.options.db}] on ${utcNow()}`);
      this.client = redisClient;
      resolve(redisClient);
    })));
  };

  get isConnected(): boolean {
    return this.client.connected;
  }
}
