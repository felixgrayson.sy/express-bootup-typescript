import moment from 'moment';

export function utcNow( ): string {
  return moment.utc().format();
}
