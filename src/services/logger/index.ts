import appRoot from 'app-root-path';
import { createLogger, format, Logger, transports } from 'winston';
import config from '../../config';

const { combine, colorize, simple } = format;

export default class Winston {
  private static instance: Winston;
  private readonly _logger: Logger;

  private constructor() {
    this._logger = createLogger({
      level: config.get('logger.level'),
      format: combine(
        colorize(),
        simple()
      ),
      transports: [
        new transports.Console({
          level: 'info',
          handleExceptions: true
        }),
        new transports.File({
          filename: `${appRoot}/logs/app.log`,
          handleExceptions: true,
          maxFiles: 10,
          maxsize: 5242880
        })
      ],
      exitOnError: false
    });
  }

  static getInstance = () => {
    if ( !Winston.instance ) {
      Winston.instance = new Winston();
    }
    return Winston.instance;
  };

  get logger() {
    return this._logger;
  }
}
