/** @format */

import { createTerminus } from '@godaddy/terminus';
import { Server } from 'http';
import config from '../../config';

function onSignal() {
  console.log('server is starting cleanup');
  return Promise.all([
    // your clean logic, like closing database connections
    // AWS SQS connection
  ]);
}

function onShutdown() {
  console.log('cleanup finished, server is shutting down');
  return Promise.all([]);
}

function onHealthCheck() {
  // checks if the system is healthy, like the db connection is live
  // resolves, if health, rejects if not
  return new Promise((resolve, reject) => {
    resolve({ health: true });
  });
}

const terminus = (server: Server) => {
  return createTerminus(server, {
    signal: 'SIGINT',
    onSignal,
    onShutdown,
    healthChecks: { '/express/api/public/health_check': onHealthCheck },
  });
};

export default terminus;
