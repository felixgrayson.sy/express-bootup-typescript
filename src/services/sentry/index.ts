/** @format */

import sentryNode from '@sentry/node';
import Winston from '../logger';
import { LogLevel } from '@sentry/types';
import { utcNow } from '../../services/utils/data-utils';

const logger = Winston.getInstance().logger;

export default class Sentry {
  private static instance: Sentry;
  private readonly _client = undefined;

  private constructor(url: string) {
    if (!url) {
      throw new Error('Sentry connection string is null or undefined');
    }

    sentryNode.init({
      dsn: url,
      onFatalError: error => logger.error(error),
      logLevel: LogLevel.Error,
    });

    this._client = sentryNode.getCurrentHub().getClient();
  }

  static getInstance(url: string) {
    if (!Sentry.instance) {
      Sentry.instance = new Sentry(url);
    }
    return Sentry.instance;
  }

  public stop(): Promise<void> {
    if (this._client) {
      return this._client.close(2000).then(function() {
        logger.info(`Sentry connection is closed at ${utcNow()}`);
        process.exit(1);
      });
    }
  }

  get sentry() {
    return sentryNode;
  }
}
