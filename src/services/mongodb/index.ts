import { Collection, Db, MongoClient, MongoClientOptions, ObjectID, ReadPreference } from 'mongodb';
import { utcNow } from '../utils/data-utils';
import Logger from '../logger';
import Sentry from '../sentry';
import config from '../../config';

const logger = Logger.getInstance().logger;
const sentry = Sentry.getInstance(config.get('sentryUrl')).sentry;

export interface IMongoDatabase {
  connect: () => Promise<void>;
  close: () => Promise<boolean>;
  collection: ( name: string ) => Collection;
  readonly objectId: ObjectID;
  readonly getDb: Db;
  collectionNames: () => Promise<Array<string>>;
  checkIndex: ( collectionName: string, indexName: string ) => Promise<boolean>;
  isConnected: () => boolean;
}

export class MongoDatabase implements IMongoDatabase {
  private readonly dbUrl: string;
  private readonly dbName: string;
  private db: Db;
  private client: MongoClient;
  private readonly dbOptions: MongoClientOptions;

  constructor( name: string, url: string, options?: MongoClientOptions ) {
    if ( !name ) {
      throw new Error('the database name is null or undefined');
    }

    if ( !url ) {
      throw new Error('the database connection string is null or undefined');
    }
    this.dbUrl = url;
    this.dbName = name;
    if ( options ) {
      this.dbOptions = options;
    }
  }

  connect = async (): Promise<void> => {
    try {
      this.client = await MongoClient.connect(this.dbUrl, this.dbOptions);
      this.db = await this.client.db(this.dbName);
      logger.info(`[MongoDB].[Info]: connected to database [${this.dbName}] on ${utcNow()}`);
    } catch ( error ) {
      const msg = `[MongoDB].[Error]: failed to connect to database on ${utcNow()}`;
      logger.info(msg);
      sentry.captureException(msg);
    }
  };

  close = async (): Promise<boolean> => {
    if ( this.client.isConnected() ) {
      try {
        await this.client.close(true);
        logger.info(`[MongoDB].[Info]: The Database[${this.dbName}] connection is closed on ${utcNow()}`);
        return true;
      } catch ( err ) {
        logger.warn(`[MongoDB].[Error]: Failed to close database connection on ${utcNow()}.[Stack]: ${JSON.stringify(err)}`);
        return false;
      }
    } else {
      logger.info(`[MongoDB].[Info]: Database has already stopped`);
      return false;
    }
  };

  collection = ( name: string ): Collection => {
    if ( !name ) {
      throw Error('the collection name is null or undefined');
    }
    return this.db.collection(name);
  };

  get objectId(): ObjectID {
    return new ObjectID();
  }

  get getDb(): Db {
    return this.db;
  }

  collectionNames = async (): Promise<Array<string>> => {
    const collections = await this.db.listCollections({}, {
      batchSize: 5,
      readPreference: ReadPreference.SECONDARY_PREFERRED
    }).toArray();
    return collections.map(value => value.name);
  };

  checkIndex = async ( collectionName: string, indexName: string ): Promise<boolean> => {
    return await this.db.collection(collectionName).indexExists(indexName);
  };

  isConnected = (): boolean => {
    return this.client.isConnected();
  };
}
