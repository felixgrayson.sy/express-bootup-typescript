import convict from 'convict';
import * as convictFormat from 'convict-format-with-validator';

convict.addFormat(convictFormat.url);

const config = convict({
  env: {
    doc: 'The application environment',
    format: [
      'production', 'development', 'test'
    ],
    default: 'development',
    env: 'NODE_ENV'
  },
  service: {
    port: {
      doc: 'The port to bind',
      format: 'port',
      default: 8080,
      env: 'PORT',
      arg: 'port'
    },
    baseUrl: {
      doc: 'The base url of the server',
      format: String,
      default: '/express/api',
      env: 'BASE_URL'
    },
    CORS: {
      doc: 'The CORS settings',
      format: Array,
      default: ['*'],
      env: 'CORS'
    }
  },
  jwks: {
    remote_url: {
      format: 'url',
      default: 'https://service.centrality.me/identity/.well-known/openid-configuration/jwks'
    },
    issuer: {
      format: String,
      default: 'https://service.centrality.me/identity'
    },
    aud: {
      format: Array,
      default: []
    }
  },
  db: {
    connectionString: {
      doc: 'The database connection string',
      format: String,
      default: 'mongodb://localhost:27017/?readPreference=primary',
      sensitive: true
    },
    name: {
      doc: 'The database name',
      format: String,
      default: 'app'
    }
  },
  logger: {
    level: {
      doc: 'The level of logger',
      format: ['debug', 'verbose', 'info', 'warn', 'error'],
      default: 'debug'
    },
    tag: {
      doc: 'The Express server tag',
      format: String,
      default: 'Express'
    }
  },
  sentryUrl: {
    doc: 'The node.js sentry connection string',
    format: 'url',
    default: 'https://{PUBLIC_KEY}@{HOST}/{PROJECT_ID}',
    env: 'SENTRY_URL'
  }
});

config.loadFile(['./settings/appsettings.json', './secrets/secrets.json']);

config.validate({ allowed: 'strict' });

export default config;
