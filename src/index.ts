/** @format */

import http, { Server } from 'http';
import moment from 'moment';
import terminus from './services/terminus';
import app from './app';
import config from './config';
import WinstonLogger from './services/logger';

async function bootUp(): Promise<void> {
  const logger = WinstonLogger.getInstance().logger;

  logger.info('STEP 6. --- Creating the HTTP Server');
  const port = config.get('service.port') || 8080;
  const server: Server = http.createServer(app);

  terminus(server);

  server.listen(port, () => {
    console.info(
      `the HTTP server is started at port:[${port}] on ${moment.utc().format()}`
    );
  });
}

bootUp();
