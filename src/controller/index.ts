import { Express } from 'express';
import version from './version';
import hello from './hello';
import config from '../config';

const init = ( app: Express ) => {
  const baseUrl = config.get('service.baseUrl');
  app.use(baseUrl, version);
  app.use(baseUrl, hello);
};

export default init;
