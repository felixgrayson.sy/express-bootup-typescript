import request from 'supertest';
import app from '../../../app';
import config from '../../../config';
import ver from '../../../version.json';

const baseUrl = config.get('service.baseUrl');

describe('GET /version endpoint', () => {
  it('should return build number', function () {
    return request(app)
      .get(`${baseUrl}/public/version`)
      .set('Accept', 'application/json')
      .expect(200)
      .then(res => {
        expect(res.body).not.toBeUndefined();
        expect(res.body.serviceName).toBe(ver.name);
      });
  });
});
