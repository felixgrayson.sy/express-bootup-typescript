import express, { Request, Response } from 'express';
import ver from '../../version.json';

const router = express.Router();

const versionHandler = ( req: Request, res: Response ) => {
  return res.status( 200 ).json( {
    serviceName: ver.name,
    buildNumber: ver.buildNumber
  } );
};

router.get( '/public/version', versionHandler );

export default router;
