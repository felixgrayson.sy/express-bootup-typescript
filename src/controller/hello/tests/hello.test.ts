import request from 'supertest';
import nock from 'nock';
import { generateJWK, issueToken } from '../../../specs/identityMocker';
import app from '../../../app';
import config from '../../../config';

const baseUrl = config.get('service.baseUrl');

beforeAll(async () => {
  const keySets = await generateJWK(); /*?*/
  nock('https://service.centrality.me')
    .persist(true)
    .get('/identity/.well-known/openid-configuration/jwks')
    .reply(200, JSON.stringify({...keySets}));
});

afterAll(() => {
  nock.restore();
});

describe('POST /hello', () => {
  it('should return hello world', function () {
    const payload = {
      email_address: 'felix+individual+test@ham.china.net.nz',
      user_id: '2ed4d909-a44b-5e44-576c-e12dc963cb20',
      iss: 'https://service.centrality.me/identity',
      client_id: 'centrality',
      aud: [
        'https://service.centrality.me/identity/resources',
        'centralityapi'
      ]
    };

    const token = issueToken(payload);

    return request(app)
      .post(`${baseUrl}/hello`)
      .send({ name: 'world'})
      .set('Authorization', 'bearer ' + token)
      .set('Accept', 'application/json')
      .expect(200)
      .then(res => {
        expect(res.body.message).not.toBeUndefined();
      });
  });
});
