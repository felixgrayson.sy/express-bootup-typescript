import Joi from '@hapi/joi';

export const schema = Joi.object().keys({
    name: Joi.string().required()
});

