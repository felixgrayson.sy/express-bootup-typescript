/** @format */

import express, { Request, Response } from 'express';
import { schema } from './schema';
import { httpCode } from '../../const';

const router = express.Router();

const helloWorldHandler = (req: Request, res: Response) => {
  const { value, error } = schema.validate(req.body);
  if (error) {
    return res
      .status(httpCode.Bad_Request)
      .json({ message: 'invalid payload' });
  }

  return res
    .status(200)
    .json({
      message: `Hello ${value.name}! Welcome to Express App hello world example.`,
    });
};

router.post('/hello', helloWorldHandler);

export default router;
