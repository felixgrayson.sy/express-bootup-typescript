import express from 'express';
import compression from 'compression';
import helmet from 'helmet';
import Cors from 'cors';
import bodyParser from 'body-parser';
import Winston from './services/logger';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import jwt from 'express-jwt';
import responseTime from 'response-time';
import { expressJwtSecret } from 'jwks-rsa';
import DotEnv from 'dotenv';
import controller from './controller';
import errorHandler from './middleware/errorHandler';
import config from './config';


DotEnv.config();
const app = express();
const logger = Winston.getInstance().logger;

app.use( morgan( 'combined', {
  stream: {
    write: ( message ) => {
      logger.info( message );
    }
  }
} ) );

//
// Register CORS
// ----------------------------------------------------------------------------------
logger.info( 'STEP 1. --- Register the CORS' );
const corsWhiteList: Array<string> = config.get( 'service.CORS' );
const corsOptions = {
  origin: function ( origin, callback ) {
    if ( corsWhiteList.length === 1 && corsWhiteList[ 0 ] === '*' ) {
      callback( undefined, true );
    } else if ( corsWhiteList.indexOf( origin ) !== -1 ) {
      callback( undefined, true );
    } else {
      callback( new Error( `the origin: ${ origin } is not allowed by CORS` ) );
    }
  },
  methods: [ 'GET', 'POST', 'PUT', 'DELETE', 'OPTIONS' ],
  allowedHeaders: [ 'Content-Type', 'Authorization', 'X-Apollo-Tracing', 'Content-Length', 'Accept-Language', 'Accept-Encoding' ],
  credentials: true,
  optionsSuccessStatus: 204
};

app.options( '*', Cors( corsOptions ) );

//
// Register Node.js middleware
// ----------------------------------------------------------------------------------
logger.info( 'STEP 2. --- Register Node.js middleware' );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( { extended: false } ) );
app.use( helmet() );
app.use( compression() );
app.use( cookieParser() );
app.use( responseTime() );

//
// Authentication
// ----------------------------------------------------------------------------------
logger.info( `STEP 3. --- Register Authentication middleware` );
const jwtUrl = config.get( 'jwks.remote_url' );
const issuer = config.get( 'jwks.issuer' );
const audience = config.get( 'jwks.aud' );
app.use( jwt( {
  secret: expressJwtSecret( {
    cache: true,
    cacheMaxAge: 60,
    rateLimit: true,
    jwksRequestsPerMinute: 2000,
    jwksUri: jwtUrl,
    handleSigningKeyError: err => {
      logger.error( `[JWT].[Error]: ${ err }` );
    }
  } ),
  audience,
  issuer,
  algorithms: [ 'RS256' ]
} ).unless( { path: [ /(public)/, /(internal)/ ] } ) );

//
// Register routers & controllers
// ----------------------------------------------------------------------------------
logger.info( `STEP 4. --- Controller Registration` );
controller( app );

//
// Register global error handler
// ----------------------------------------------------------------------------------
logger.info( `STEP 5. --- Global Error Handler Registration` );
app.use( errorHandler );

export default app;
