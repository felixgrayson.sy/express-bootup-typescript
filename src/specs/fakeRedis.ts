import redis from 'redis-mock';
import { IRedisCli } from '../services/redis';

class FakeRedis implements IRedisCli {
  static instance: FakeRedis;
  client;
  options;
  private constructor() {
  }

  static getInstance() {
    if ( !FakeRedis.instance ) {
      FakeRedis.instance = new FakeRedis();
    }
    return FakeRedis.instance;
  }

  connect = async (): Promise<any> => {
    const redisClient = redis.createClient();
    this.client = redisClient;
    return Promise.resolve(redisClient);
  };

  get isConnected(): boolean {
    return true;
  }

}

export default FakeRedis;
