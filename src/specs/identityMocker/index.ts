import jwt from 'jsonwebtoken';
import fs from 'fs';
import path from 'path';
import uuid from 'uuid/v1';
import rsaPemToJwk from 'rsa-pem-to-jwk';
import crypto from 'crypto';

interface Token {
  user_id: string;
  email_address: string;
  iss: string;
  aud: Array<string>;
  client_id: string;
}



const kid = crypto.randomBytes(80).toString('hex'); /*?*/

const issueToken = ( payload: Token ): string => {
  if ( !payload ) {
    return undefined;
  }

  const { iss, aud, ...tokenPayload } = payload;
  const cert = fs.readFileSync(path.join(__dirname, '../private.pem'));
  return jwt.sign(tokenPayload, cert, {
    algorithm: 'RS256',
    keyid: kid,
    issuer: iss,
    expiresIn: '1h',
    audience: aud,
    jwtid: uuid()
  });
};

const generateJWK = async () => {
  try {
    const privatePEM = fs.readFileSync(path.join(__dirname, '../private.pem'));
    const jwk = rsaPemToJwk(privatePEM, { use: 'sig', alg: 'RS256', kid}, 'public');
    const keys = [];
    keys.push(jwk);
    return { keys };
  } catch ( err ) {
    return undefined;
  }
};

export { issueToken, generateJWK };
