const { FakeExpressDB } = require('../../dist/specs/fakeMongo');

module.exports = async function () {
  const fakeExpressDb = FakeExpressDB.getInstance();
  await fakeExpressDb.close();
};
