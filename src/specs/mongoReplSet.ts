import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { Collection, Db, MongoClient, ObjectID } from 'mongodb';
import Winston from '../services/logger';
import { IMongoDatabase } from '../services/mongodb';


const logger = Winston.getInstance().logger;

export default class MongoCluster implements IMongoDatabase {
  db: Db;
  replSet: MongoMemoryReplSet;
  client: MongoClient;
  dbName: string;

  constructor() {
    // @ts-ignore
    this.replSet = new MongoMemoryReplSet({
      binary: {
        version: '4.0.1'
      },
      instanceOpts: [
        {
          storageEngine: 'wiredTiger',  // same storage engine options
        },
        // each entry will result in a MongoMemoryServer
      ],
      replSet: {
        storageEngine: 'wiredTiger'
      }
    });
  }

  checkIndex = async ( collectionName: string, indexName: string ): Promise<boolean> => {
    return await this.db.collection(collectionName).indexExists(indexName);
  };

  close = async () => {
    return this.replSet.stop();
  };

  collection( name: string ): Collection {
    if ( !name ) {
      throw new Error('Failed to fetch the collection, because the collection name is null or undefined');
    }
    return this.db.collection(name);
  }

  collectionNames: () => Promise<Array<string>>;

  async connect(): Promise<void> {
    try {
      await this.replSet.waitUntilRunning();
      const connectionString = await this.replSet.getConnectionString();
      this.client = await MongoClient.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true });
      this.db = await this.client.db(this.dbName);
    } catch ( err ) {
      logger.info(err);
    }
  }

  get getDb(): Db {
    if ( this.client.isConnected() ) {
      return this.db;
    }
    return undefined;
  }

  isConnected = () => this.client.isConnected();

  get objectId(): ObjectID {
    return new ObjectID();
  }
}
