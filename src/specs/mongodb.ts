import { Collection, Db, MongoClient, ObjectID, ReadPreference } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import Logger from '../services/logger';
import { IMongoDatabase } from '../services/mongodb';

const logger = Logger.getInstance().logger;

export class MongoInMemoryService implements IMongoDatabase {
  readonly dbName: string;
  db: Db;
  client: MongoClient;
  Mango: MongoMemoryServer;

  constructor( name: string, port?: number ) {
    this.dbName = name;
    this.Mango = new MongoMemoryServer({
      instance: {
        dbName: name,
        port: port || 27017
      },
      binary: {
        version: '4.0.1'
      }
    });
  }

  async connect(): Promise<void> {
    try {
      const connectionString = await this.Mango.getConnectionString();
      this.client = await MongoClient.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true });
      this.db = await this.client.db(this.dbName);
    } catch ( err ) {
      logger.info(err);
    }
  }

  collection( name: string ): Collection {
    if ( !name ) {
      throw new Error('Failed to fetch the collection, because the collection name is null or undefined');
    }
    return this.db.collection(name);
  }

  close = async (): Promise<boolean> => {
    return this.Mango.stop();
  };

  get getDb() {
    if ( this.client.isConnected() ) {
      return this.db;
    } else {
      return undefined;
    }
  }

  get objectId(): ObjectID {
    return new ObjectID();
  }

  collectionNames = async (): Promise<Array<string>> => {
    const collections = await this.db.listCollections({}, {
      batchSize: 5,
      readPreference: ReadPreference.PRIMARY
    }).toArray();
    return collections.map(value => value.name);
  };

  checkIndex = async ( collectionName: string, indexName: string ): Promise<boolean> => {
    return await this.db.collection(collectionName).indexExists(indexName);
  };

  isConnected = (): boolean => {
    return this.client.isConnected();
  };
}

export default MongoInMemoryService;
