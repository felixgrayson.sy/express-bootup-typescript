import { FakeExpressClusterDB } from '../index';

let fakeDb;
beforeAll(async () => {
  fakeDb = await FakeExpressClusterDB.getInstance();
  await fakeDb.connect();
});

describe('Cluster DB Mockup Test', () => {
  it('should connect to DB', async function () {
    expect(fakeDb.isConnected).toBeTruthy();
  });
});
