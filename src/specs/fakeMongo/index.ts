import Mongodb from '../mongodb';
import MongoReplSet from '../mongoReplSet';

class FakeExpressDB extends Mongodb {
  private static instance: FakeExpressDB;

  private constructor() {
    super('FakeExpressDB');
  }

  static getInstance() {
    if ( !FakeExpressDB.instance ) {
      FakeExpressDB.instance = new FakeExpressDB();
    }
    return FakeExpressDB.instance;
  }
}

class FakeExpressClusterDB extends MongoReplSet {
  private static instance: FakeExpressClusterDB;

  private constructor() {
    super();
  }

  static getInstance() {
    if ( !FakeExpressClusterDB.instance ) {
      FakeExpressClusterDB.instance = new FakeExpressClusterDB();
    }
    return FakeExpressClusterDB.instance;
  }
}

export { FakeExpressDB, FakeExpressClusterDB };
