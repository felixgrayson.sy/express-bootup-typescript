#!/usr/bin/env bash
set -exec

: "${SERVICE_NAME:?SERVICE_NAME is required}"
: "${BUILD_NUMBER:?BUILD_NUMBER is required}"

IMAGE_NAME="felixgrayson/${SERVICE_NAME}:1.0.${BUILD_NUMBER}"

# Temp solution find a proper solution
sed "s|newBuildNumber|1.0.${BUILD_NUMBER}|g" version.json > ./dist/version.json

docker build -t "${IMAGE_NAME}" -f docker/build/Dockerfile .