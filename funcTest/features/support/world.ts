/** @format */

import rp, { RequestPromiseOptions } from 'request-promise-native';
import { setWorldConstructor } from 'cucumber';
import _ from 'lodash';

class CustomWorld {
  constructor() {}

  async get(uri: string, options?: RequestPromiseOptions) {
    try {
      const opts = _.merge(options, { json: true });
      const result = await rp.get(uri, opts);
      return _.merge(result, { success: true });
    } catch (err) {
      const msg = _.get('error', err);
      const statusCode = _.get('statusCode', err);
      return { success: false, statusCode, message: msg };
    }
  }

  async post(uri: string, data: object, options?: RequestPromiseOptions) {
    try {
      const opts = _.merge(options, { json: true, body: data });
      const result = await rp.post(uri, opts);
      if (result.success) return result;
      return { success: true, data: result };
    } catch (err) {
      const msg = _.get('error', err);
      const statusCode = _.get('statusCode', err);
      return { success: false, statusCode, message: msg };
    }
  }
}

setWorldConstructor(CustomWorld);
