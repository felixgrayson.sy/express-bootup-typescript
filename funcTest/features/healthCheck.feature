Feature: Health Check
  Scenario: Calling the health check API
    Given the app is running
    When make get request on '/public/version'
    Then status 200 is returned
    Then Version matches correct number