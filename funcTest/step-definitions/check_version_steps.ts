/** @format */

import { binding, given, when, then }from 'cucumber-tsflow';
import rp, { RequestPromiseOptions } from 'request-promise-native';
import { assert } from 'chai';

@binding()
class Check_version_steps {
  private statusCode: number;
  private readonly options: RequestPromiseOptions;

   constructor() {
     this.statusCode = 500;
     this.options = {
       baseUrl: 'http://localhost:8080',
       json: true,
       resolveWithFullResponse: true
     }
  }

  @given(/the app is running/)
  public async anRunningApp(): Promise<void> {
     try{
       const result = await rp.get('/public/healthCheck', this.options);
       assert.equal(result.statusCode, 200);
     } catch (err) {

     }
    
  }

  @when(/^make get request on '\/public\/version'$/)
  public async callingVersionEndpoint(): Promise<void> {
     const result = await rp.get('/public/version', this.options);
  }

  @then(/^status (\d+) is returned$/)
  public async checkStatusCode(statusCode: number) {

  }

  @then(/^Version matches correct number$/)
  public async matchVersionNumber() {

  }
}
