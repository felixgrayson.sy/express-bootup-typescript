/** @format */
module.exports = function(wallaby) {
  return {
    files: [
      './src/**/*.ts',
      './secrets/**/*.json',
      './settings/**/*.json',
      '!./src/**/tests/**/*.test.ts',
      './src/version.json',
      './package.json',
      './src/**/*.png',
    ],

    tests: ['./src/**/*.test.ts', './src/**/tests/**/*.test.ts'],

    env: {
      type: 'node',
      runner: 'node',
    },

    testFramework: 'jest',

    workers: {
      restart: true,
    },

    compiler: {
      '**/*.ts?(x)': wallaby.compilers.typeScript({
        typescript: require('typescript'),
        module: 'commonjs',
      }),
    },

    filesWithNoCoverageCalculated: [],

    setup: function(wallaby) {
      const jestConfig = require('./package.json').jest;
      // for example:
      // jestConfig.globals = { '__DEV__': true };
      // jestConfig.globalSetup = './mongoMemoryServer/mongodb.js';
      // jestConfig.globalTeardown = './mongoMemoryServer/teardown.js';
      // jestConfig.testEnvironment = './mongoMemoryServer/mongo-environment.js';

      wallaby.testFramework.configure(jestConfig);
    },
  };
};
