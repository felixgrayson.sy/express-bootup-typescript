module.exports = {
  "globals": {
    "ts-jest": {
      "tsConfig": "tsconfig.json"
    }
  },
  "moduleFileExtensions": [
    "ts",
    "js",
    "json",
    "node"
  ],
  "testMatch": [
    "<rootDir>/src/**/*.test.(ts|js)"
  ],
  "transform": {
    "^.+\\.(ts|tsx)$": "ts-jest"
  },
  "reporters": [
    "default",
    ["jest-junit", { suiteName: "Express Bootstrap Test"}],
    ["./node_modules/jest-html-reporter", {
      "pageTitle": "Test Report",
      "outputPath": "test-report/index.html"
    }]
  ],
  "testEnvironment": "node",
  "globalSetup": "<rootDir>/src/specs/setup.js",
  "globalTeardown": "<rootDir>/src/specs/teardown.js"
};
